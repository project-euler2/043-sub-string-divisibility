import time
import itertools
start_time = time.time()

def find_sum_divisble_pandigital():
    pandigitals =  [''.join(l) for l in itertools.permutations('0123456789')]
    divisible_pandigital = []
    for pandigital in pandigitals:
        if (int(str(pandigital)[1:4]) % 2 == 0) and \
            (int(str(pandigital)[2:5]) % 3 == 0) and \
            (int(str(pandigital)[3:6]) % 5 == 0) and \
            (int(str(pandigital)[4:7]) % 7 == 0) and \
            (int(str(pandigital)[5:8]) % 11 == 0) and \
            (int(str(pandigital)[6:9]) % 13 == 0) and \
            (int(str(pandigital)[7:]) % 17 == 0):
            divisible_pandigital.append(int(pandigital))
    return sum(divisible_pandigital)

print(find_sum_divisble_pandigital())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )